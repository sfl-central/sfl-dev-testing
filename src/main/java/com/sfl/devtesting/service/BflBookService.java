package com.sfl.devtesting.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sfl.devtesting.entity.BflBook;
import com.sfl.devtesting.exception.BflCentralException;

@Component
public interface BflBookService {
	
	List<BflBook> getAllBooks() throws BflCentralException;
	
	BflBook getBookById(Long bookId) throws BflCentralException;
	
	BflBook createBook(BflBook book) throws BflCentralException;
	
	BflBook deleteBook(Long bookId) throws BflCentralException;
	
	BflBook updateBook(BflBook book, Long bookId) throws BflCentralException;
}

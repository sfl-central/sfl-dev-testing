package com.sfl.devtesting.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfl.devtesting.dao.BflBookDao;
import com.sfl.devtesting.entity.BflBook;
import com.sfl.devtesting.exception.BflCentralException;

@Service
public class BflBookServiceImpl implements BflBookService {
	
	@Autowired
	private BflBookDao bflBookDao;
	
	@Override
	public List<BflBook> getAllBooks() throws BflCentralException {
		List<BflBook> allBooks = new ArrayList<>();
		allBooks = bflBookDao.getAllBooks();
		return allBooks;
	}

	@Override
	public BflBook getBookById(Long bookId) throws BflCentralException {
		BflBook getBookId = new BflBook();
		getBookId = bflBookDao.getBookById(bookId);
		return getBookId;
	}

	@Override
	public BflBook createBook(BflBook book) throws BflCentralException {
		BflBook createBook = new BflBook();
		createBook = bflBookDao.createBook(book);
		return createBook;
	}

	@Override
	public BflBook deleteBook(Long bookId) throws BflCentralException {
		BflBook deleteBook = new BflBook();
		deleteBook = bflBookDao.deleteBook(bookId);
		return deleteBook;
	}

	@Override
	public BflBook updateBook(BflBook book, Long bookId) throws BflCentralException {
		BflBook updateBook = new BflBook();
		updateBook = bflBookDao.updateBook(book, bookId);
		return updateBook;
	}
	

	
}

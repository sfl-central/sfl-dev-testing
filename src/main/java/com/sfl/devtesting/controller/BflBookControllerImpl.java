/**
 * 
 */
package com.sfl.devtesting.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfl.devtesting.bean.BflBookRequestBean;
import com.sfl.devtesting.bean.BflBookResponseBean;
import com.sfl.devtesting.entity.BflBook;
import com.sfl.devtesting.exception.BflCentralException;
import com.sfl.devtesting.service.BflBookService;
import com.sfl.devtesting.util.BflBookUtil;

/**
 * @author Abhijay Bsht
 *
 */
@RestController
@RequestMapping("api/bfl/")
public class BflBookControllerImpl implements BflBookController {

	@Autowired
	private BflBookService bflBookService;

	@GetMapping("books/find/all")
	public ResponseEntity<BflBookResponseBean> getAllBook() {
		BflBookResponseBean bflBookResponseBean = new BflBookResponseBean();
		List<BflBook> allBooks = new ArrayList<BflBook>();
		try {
			allBooks = bflBookService.getAllBooks();
		} catch (BflCentralException e) {
			bflBookResponseBean.setBflContextBean(BflBookUtil.failureProcessing(e.getMessage()));
		}
		bflBookResponseBean.setBflContextBean(BflBookUtil.sucessProcessing("All books has been fetch from Database"));
		bflBookResponseBean.setAllBooks(allBooks);
		return ResponseEntity.ok(bflBookResponseBean);
	}
	
	@GetMapping("books/find/{bookId}")
	public ResponseEntity<BflBookResponseBean> getBookById(@PathVariable Long bookId) {
		BflBookResponseBean bflBookResponseBean = new BflBookResponseBean();
		BflBook book = new BflBook(); 
		try {
			book = bflBookService.getBookById(bookId);
		} catch (BflCentralException e) {
			bflBookResponseBean.setBflContextBean(BflBookUtil.failureProcessing(e.getMessage()));
		}
		bflBookResponseBean.setBflContextBean(BflBookUtil.sucessProcessing("Book has been fetch from Database with ID: "+bookId));
		bflBookResponseBean.setBook(book);
		return ResponseEntity.ok(bflBookResponseBean);
	}
	

	@PostMapping("books/add")
	public ResponseEntity<BflBookResponseBean> createBook(@RequestBody BflBookRequestBean req) {
		BflBookResponseBean bflBookResponseBean = new BflBookResponseBean();
		BflBook createBook = new BflBook(); 
		try {
			createBook = bflBookService.createBook(req.getBook());
		} catch (BflCentralException e) {
			bflBookResponseBean.setBflContextBean(BflBookUtil.failureProcessing(e.getMessage()));
		}
		bflBookResponseBean.setBflContextBean(BflBookUtil.sucessProcessing("New book has been created with ID"+createBook.getBookId()));
		bflBookResponseBean.setBook(createBook);
		return ResponseEntity.ok(bflBookResponseBean);
	}
	
	@DeleteMapping("books/delete/{bookId}")
	public ResponseEntity<BflBookResponseBean> deleteBook(@PathVariable Long bookId) {
		BflBookResponseBean bflBookResponseBean = new BflBookResponseBean();
		BflBook deleteBook = new BflBook(); 
		try {
			deleteBook = bflBookService.deleteBook(bookId);
		} catch (BflCentralException e) {
			bflBookResponseBean.setBflContextBean(BflBookUtil.failureProcessing(e.getMessage()));
		}
		bflBookResponseBean.setBflContextBean(BflBookUtil.sucessProcessing("This Book has been deleted. "+bookId));
		bflBookResponseBean.setBook(deleteBook);
		return ResponseEntity.ok(bflBookResponseBean);
	}
	
	@PutMapping("books/update/{bookId}")
	public ResponseEntity<BflBookResponseBean> updateBook(@RequestBody BflBookRequestBean req, @PathVariable Long bookId) {
		BflBookResponseBean bflBookResponseBean = new BflBookResponseBean();
		BflBook updateBook = new BflBook();
		
		try {
			updateBook = bflBookService.updateBook(req.getBook(), bookId);
		} catch (BflCentralException e) {
			bflBookResponseBean.setBflContextBean(BflBookUtil.failureProcessing(e.getMessage()));
		}
		bflBookResponseBean.setBflContextBean(BflBookUtil.sucessProcessing("This Book has been update with ID: "+bookId));
		bflBookResponseBean.setBook(updateBook);
		return ResponseEntity.ok(bflBookResponseBean);
	}
}

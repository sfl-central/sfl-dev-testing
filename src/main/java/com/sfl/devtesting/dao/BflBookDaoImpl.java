package com.sfl.devtesting.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sfl.devtesting.constant.BflBookConstant;
import com.sfl.devtesting.constant.BflBookErrorMessage;
import com.sfl.devtesting.entity.BflBook;
import com.sfl.devtesting.exception.BflCentralException;
import com.sfl.devtesting.repository.BflBookRepository;

@Component
public class BflBookDaoImpl implements BflBookDao {

	@Autowired
	private BflBookRepository bflBookRepository;
	
	@Override
	public List<BflBook> getAllBooks() throws BflCentralException{
		List<BflBook> allBooks = new ArrayList<>();
		try {
			allBooks = bflBookRepository.findAll();
		} catch (Exception e) {
			throw new BflCentralException(BflBookErrorMessage.ErrorCode01, BflBookErrorMessage.ErrorMessage01, e); 
		}
		return allBooks;
	}
	
	@Override
	public BflBook getBookById(Long bookId) throws BflCentralException{
		BflBook book = new BflBook();
		try {
			book = bflBookRepository.findById(bookId).get();
		} catch (Exception e) {
			throw new BflCentralException(BflBookErrorMessage.ErrorCode01, BflBookErrorMessage.ErrorMessage01, e); 
		}
		return book;
	}
	
	@Override
	public BflBook createBook(BflBook book) throws BflCentralException{
		BflBook createBook = new BflBook();
		try {
			
			createBook.setBookId(BflBookConstant.NULL);
			createBook = bflBookRepository.save(book);
		
		} catch (Exception e) {
			throw new BflCentralException(BflBookErrorMessage.ErrorCode01, BflBookErrorMessage.ErrorMessage01, e); 
		}
		return book;
	}
	

	@Override
	public BflBook deleteBook(Long bookId) throws BflCentralException{
		BflBook deleteBook = new BflBook();
		try {
			
			bflBookRepository.deleteById(bookId) ;
		
		} catch (Exception e) {
			throw new BflCentralException(BflBookErrorMessage.ErrorCode01, BflBookErrorMessage.ErrorMessage01, e); 
		}
		return deleteBook;
	}
	
	@Override
	public BflBook updateBook(BflBook book, Long bookId) throws BflCentralException{
		BflBook updateBook = new BflBook();
		try {
			
			updateBook = bflBookRepository.findById(bookId).get();
			updateBook.setBookName(book.getBookName());
			updateBook.setBookAuthor(book.getBookAuthor());
			updateBook = bflBookRepository.save(updateBook);
		
		} catch (Exception e) {
			throw new BflCentralException(BflBookErrorMessage.ErrorCode01, BflBookErrorMessage.ErrorMessage01, e); 
		}
		return updateBook;
	}
}

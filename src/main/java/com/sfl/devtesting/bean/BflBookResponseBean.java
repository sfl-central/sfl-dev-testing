package com.sfl.devtesting.bean;

import java.util.List;

import com.sfl.devtesting.entity.BflBook;

import lombok.Data;

@Data
public class BflBookResponseBean {

	private BflContextBean bflContextBean;
	
	private BflBook book;
	
	private List<BflBook> allBooks;
	
}

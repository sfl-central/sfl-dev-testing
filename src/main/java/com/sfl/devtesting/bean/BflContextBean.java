package com.sfl.devtesting.bean;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class BflContextBean {
	
	private String status;
	
	private String message;
	
	private OffsetDateTime responseDate;

}

package com.sfl.devtesting.bean;

import com.sfl.devtesting.entity.BflBook;

import lombok.Data;

@Data
public class BflBookRequestBean {

	private BflContextBean bflContextBean;
	
	private BflBook book;
}

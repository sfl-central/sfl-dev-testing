package com.sfl.devtesting.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="BFL_BOOK_TAB")
public class BflBook implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BFL_BOOK_ID_COL")
	private Long bookId;
	
	@Column(name = "BFL_BOOK_NAME_COL")
	private String bookName;
	
	@Column(name = "BFL_BOOK_AUTHOR_COL")
	private String bookAuthor;
	
	
}

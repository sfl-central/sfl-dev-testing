package com.sfl.devtesting.util;

import com.sfl.devtesting.bean.BflContextBean;
import com.sfl.devtesting.constant.BflBookConstant;

public class BflBookUtil {
	
	public static BflContextBean sucessProcessing(String message) {
		BflContextBean success = new BflContextBean();
		success.setMessage(message);
		success.setStatus(BflBookConstant.success);
		success.setResponseDate(java.time.OffsetDateTime.now());
		return success;
	}
	
	public static BflContextBean failureProcessing(String message) {
		BflContextBean fail = new BflContextBean();
		fail.setStatus(BflBookConstant.failure);
		fail.setMessage(message);  
		fail.setResponseDate(java.time.OffsetDateTime.now());
		return fail;
	}
	
}

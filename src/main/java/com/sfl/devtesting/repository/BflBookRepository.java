package com.sfl.devtesting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.sfl.devtesting.entity.BflBook;

@Component
public interface BflBookRepository extends JpaRepository<BflBook, Long> {

}
